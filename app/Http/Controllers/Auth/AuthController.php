<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index(){
        // return view('auth.login');
        return redirect('https://intranet.nap.net.id/sso/');
    }

    public function login(Request $request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect('/surat');
        }
        return redirect('/')->with('message','email atau password salah !');
    }
    public function postloginSSO($q){
        //kalau pakai SSO
        $email =base64_decode($q);
        $password='-';
        // $email=$request->email;
        // $password=$request->password;
        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        $dologin=Auth::attempt($credentials);
        if($dologin){
            return redirect('/surat');
        }
        else{
            return redirect('https://intranet.nap.net.id/sso/signout'); //kalau pakai sso
        }
    }
        
    public function logout(Request $request){
        Auth::logout();
        // return redirect('/');
        return redirect('https://intranet.nap.net.id/sso/signout');
    }
    
}
